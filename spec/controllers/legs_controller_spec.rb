require 'spec_helper'

describe LegsController do

  subject(:leg) { FactoryGirl.build(:leg) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new leg" do
      expect {
        permit_all_parameters do
        post :create, {:leg => FactoryGirl.attributes_for(:leg), signed: true}, session
        end
      }.to change{Leg.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a leg" do
      leg = FactoryGirl.build(:leg)
      leg.start_address FactoryGirl.build(:address)
      leg.end_address FactoryGirl.build(:address)
      leg.transporter FactoryGirl.build(:transporter)
      leg.save

      get :show, id: leg.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a leg" do
      leg.save
      get :edit, id: leg.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a Leg" do
      leg.save!
      leg.update_attributes(estimated_mileage: 25, actual_mileage: 30, paid: true)
      patch :update, id: leg.to_param

      expect(flash[:notice]).to eq("Successfully updated Leg!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a leg" do
      leg.trip_id = "2"
      leg.save!
      delete :destroy, id: leg.to_param

      expect { change(Leg, :count).by(-1) }
      expect(response).to redirect_to(legs_path)
      expect(flash[:notice]).to eq("Leg successfully deleted!")
    end
  end

end
