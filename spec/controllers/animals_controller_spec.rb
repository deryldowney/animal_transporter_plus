require 'spec_helper'

describe AnimalsController do

  subject(:animal) { FactoryGirl.build(:animal) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new animal" do
      expect {
        permit_all_parameters do
        post :create, {:animal => FactoryGirl.attributes_for(:animal), signed: true}, session
        end
      }.to change{Animal.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a animal" do
      animal = FactoryGirl.build(:animal)
      animal.shelter FactoryGirl.build(:shelter)
      animal.adopter FactoryGirl.build(:adopter)
      animal.save

      get :show, id: animal.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a animal" do
      animal.save
      get :edit, id: animal.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a Animal" do
      animal.save!
      animal.update_attributes(breed: "Suckerfish")
      patch :update, id: animal.to_param

      expect(flash[:notice]).to eq("Successfully updated Animal!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a animal" do
      animal.save!
      delete :destroy, id: animal.to_param

      expect { change(Animal, :count).by(-1) }
      expect(response).to redirect_to(animals_path)
      expect(flash[:notice]).to eq("Animal successfully deleted!")
    end
  end

end
