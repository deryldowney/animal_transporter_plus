require 'spec_helper'

describe TransportersController do

  subject(:transporter) { FactoryGirl.build(:transporter) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new transporter" do
      expect {
        permit_all_parameters do
        post :create, {:transporter => FactoryGirl.attributes_for(:transporter), signed: true}, session
        end
      }.to change{Transporter.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a transporter" do
      transporter = FactoryGirl.build(:transporter)
      transporter.address FactoryGirl.build(:address)
      transporter.save

      get :show, id: transporter.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a transporter" do
      transporter.save
      get :edit, id: transporter.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a transporter" do
      transporter.save!
      transporter.update_attributes(first_name: "Some", last_name: "Transporter")
      patch :update, id: transporter.to_param

      expect(flash[:notice]).to eq("Successfully updated Transporter!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a transporter" do
      transporter.save!
      delete :destroy, id: transporter.to_param

      expect { change(Transporter, :count).by(-1) }
      expect(response).to redirect_to(transporters_path)
      expect(flash[:notice]).to eq("Transporter successfully deleted!")
    end
  end

end
