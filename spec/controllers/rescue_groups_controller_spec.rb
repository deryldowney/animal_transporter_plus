require 'spec_helper'

describe RescueGroupsController do

  before(:each) do
    @current_user = FactoryGirl.build_stubbed(:user)
    controller.stub(:current_user).and_return(@current_user)
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new rescue_group" do
      expect {
        permit_all_parameters do

          post :create, {rescue_group: FactoryGirl.attributes_for(:rescue_group), signed: true }, session

        end
      }.to change{@current_user.rescue_groups.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a rescue_group" do
      rescue_group = FactoryGirl.create(:rescue_group)

      get :show, id: rescue_group.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a rescue_group" do
      @current_user.rescue_groups << FactoryGirl.build(:rescue_group)

      get :edit, id: @current_user.rescue_groups.first

      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a rescue_group" do
      rescue_group = FactoryGirl.create(:rescue_group)
      rescue_group.update_attributes(name: "Updated Rescue Group")

      patch :update, id: rescue_group.to_param

      expect(flash[:notice]).to eq("Successfully updated Rescue Group!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a rescue_group" do
      rescue_group = FactoryGirl.create(:rescue_group)

      expect { delete :destroy, id: rescue_group.to_param }.to change{ RescueGroup.count }.by(-1)
      expect(response).to redirect_to(rescue_groups_path)
      expect(flash[:notice]).to eq("Rescue Group successfully deleted!")
    end
  end

end
