require 'spec_helper'

describe UsersController do

  subject(:user) { FactoryGirl.build(:user) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new user" do
      expect {
        permit_all_parameters do
          post :create, {:user => FactoryGirl.attributes_for(:user), signed: true}, session
        end
      }.to change{User.count}.by(1)
    end
  end

  describe "GET 'edit'" do
    it "edits a user" do
      user.save!
      get :edit, id: user.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'", js: true do
    it "updates a user" do
      user.save!

      expect {
        permit_all_parameters do
          user.update_attributes(full_name: "Some Updated User")
          patch :update, { id: user.to_param, signed: true}, session
        end
      }.to change{user.reload && user.full_name}.to("Some Updated User")
    end
  end


  describe "DELETE 'destroy'" do
    it "deletes a user" do
      user.save!
      delete :destroy, id: user.to_param

      expect { change(User, :count).by(-1) }
      expect(response).to redirect_to(users_path)
      expect(flash[:notice]).to eq("User successfully deleted!")
    end
  end

end
