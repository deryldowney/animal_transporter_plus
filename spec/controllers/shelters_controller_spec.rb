require 'spec_helper'

describe SheltersController do

  subject(:shelter) { FactoryGirl.build(:shelter) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new shelter" do
      expect {
        permit_all_parameters do
        post :create, {:shelter => FactoryGirl.attributes_for(:shelter), signed: true}, session
        end
      }.to change{Shelter.count}.by(1)
    end

    it "fails to create an invalid Shelter" do
      shelter = FactoryGirl.build(:shelter)
      shelter[:name] = nil
      post :create, { :shelter => shelter, signed: true}, session
      expect(shelter).to_not be_valid
      expect(shelter).to_not be_persisted
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a shelter" do
      shelter = FactoryGirl.build(:shelter)
      shelter.address FactoryGirl.build(:address)
      shelter.save

      get :show, id: shelter.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a shelter" do
      shelter.save
      get :edit, id: shelter.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a Shelter" do
      shelter.save!
      shelter.update_attributes(name: "Some Updated Shelter")
      patch :update, id: shelter.to_param

      expect(flash[:notice]).to eq("Successfully updated Shelter!")
    end

    it "fails to update a shelter with invalid information" do
      shelter.save!
      shelter[:name] = nil
      patch :update, id: shelter.to_param, signed: true

      expect(shelter).to be_invalid
      expect(shelter.updated_at_changed?).to be_false
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a shelter" do
      shelter.save!
      delete :destroy, id: shelter.to_param

      expect { change(Shelter, :count).by(-1) }
      expect(response).to redirect_to(shelters_path)
      expect(flash[:notice]).to eq("Shelter successfully deleted!")
    end
  end

end
