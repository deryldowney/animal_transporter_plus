require 'spec_helper'

describe PullersController do

  subject(:puller) { FactoryGirl.build(:puller) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new puller" do
      expect {
        permit_all_parameters do
        post :create, {:puller => FactoryGirl.attributes_for(:puller), signed: true}, session
        end
      }.to change{Puller.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a puller" do
      puller = FactoryGirl.build(:puller)
      puller.address FactoryGirl.build(:address)
      puller.save

      get :show, id: puller.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a puller" do
      puller.save
      get :edit, id: puller.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a Puller" do
      puller.save!
      puller.update_attributes(first_name: "Some", last_name: "Updated Puller")
      patch :update, id: puller.to_param

      expect(flash[:notice]).to eq("Successfully updated Puller!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a puller" do
      puller.save!
      delete :destroy, id: puller.to_param

      expect { change(Puller, :count).by(-1) }
      expect(response).to redirect_to(pullers_path)
      expect(flash[:notice]).to eq("Puller successfully deleted!")
    end
  end

end
