require 'spec_helper'

describe TripsController do

  subject(:trip) { FactoryGirl.build(:trip) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new trip" do
      expect {
        permit_all_parameters do
        post :create, {:trip => FactoryGirl.attributes_for(:trip), signed: true}, session
        end
      }.to change{Trip.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a trip" do
      trip = FactoryGirl.build(:trip)
      trip.start_address FactoryGirl.build(:address)
      trip.end_address FactoryGirl.build(:address)
      trip.save

      get :show, id: trip.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a trip" do
      trip.save
      get :edit, id: trip.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates a Trip" do
      trip.save!
      trip.update_attributes(estimated_mileage: 25, actual_mileage: 30, paid_transport: true)
      patch :update, id: trip.to_param

      expect(flash[:notice]).to eq("Successfully updated Trip!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a trip" do
      trip.save!
      delete :destroy, id: trip.to_param

      expect { change(Trip, :count).by(-1) }
      expect(response).to redirect_to(trips_path)
      expect(flash[:notice]).to eq("Trip successfully deleted!")
    end
  end

end
