require 'spec_helper'

describe AdoptersController do

  before(:each) { current_user = controller.stub(:current_user).and_return(FactoryGirl.build_stubbed(:rescue_group)) }

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    it "creates a new adopter" do
      expect {
        permit_all_parameters do
          post :create, {:adopter => current_user.adopters.create(subject), signed: true}, session
        end
      }.to change{Adopter.count}.by(1)
    end
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "shows a adopter" do
      adopter = FactoryGirl.build(:adopter)
      adopter.address FactoryGirl.build(:address)
      adopter.save

      get :show, id: adopter.to_param

      expect(response).to be_success
    end
  end

  describe "GET 'edit'" do
    it "edits a adopter" do
      adopter.save
      get :edit, id: adopter.to_param
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    it "updates an Adopter" do
      adopter.save!
      adopter.update_attributes(first_name: "Some", last_name: "Updated Adopter")
      patch :update, id: adopter.to_param

      expect(flash[:notice]).to eq("Successfully updated Adopter!")
    end
  end

  describe "DELETE 'destroy'" do
    it "deletes a adopter" do
      adopter.save!
      delete :destroy, id: adopter.to_param

      expect { change(Adopter, :count).by(-1) }
      expect(response).to redirect_to(adopters_path)
      expect(flash[:notice]).to eq("Adopter successfully deleted!")
    end
  end

end
