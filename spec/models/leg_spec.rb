require 'spec_helper'

describe Leg do
  subject { FactoryGirl.build_stubbed(:leg) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:leg)).to be_valid
  end

  it 'will be an instance of Leg' do
    expect(subject).to be_an_instance_of Leg
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_leg = FactoryGirl.build(:leg)

    it 'will not have an id' do
      expect(the_leg.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_leg.paid = nil
      expect(the_leg.valid?).to be_false
      expect(the_leg.persisted?).to_not be_true
    end
  end
end
