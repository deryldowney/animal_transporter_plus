require 'spec_helper'

describe Shelter do
  subject { FactoryGirl.build_stubbed(:shelter) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:shelter)).to be_valid
  end

  it 'will be an instance of Shelter' do
    expect(subject).to be_an_instance_of Shelter
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_shelter = FactoryGirl.build(:shelter)

    it 'will not have an id' do
      expect(the_shelter.id).to be_blank
    end

    it 'should fail validation if required info is not present' do
      the_shelter.name = nil
      the_shelter.save
      expect(the_shelter.valid?).to be_false
      expect(the_shelter.persisted?).to_not be_true
    end
  end

end
