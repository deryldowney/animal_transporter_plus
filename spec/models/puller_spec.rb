require 'spec_helper'

describe Puller do
  subject { FactoryGirl.build_stubbed(:puller) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:puller)).to be_valid
  end

  it 'will be an instance of Puller' do
    expect(subject).to be_an_instance_of Puller
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_puller = FactoryGirl.build(:puller)

    it 'will not have an id' do
      expect(the_puller.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_puller.first_name = nil
      the_puller.address = nil
      the_puller.save
      expect(the_puller.valid?).to be_false
      expect(the_puller.persisted?).to_not be_true
    end
  end

end
