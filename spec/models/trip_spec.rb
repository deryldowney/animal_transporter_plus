require 'spec_helper'

describe Trip do
  subject { FactoryGirl.build_stubbed(:trip) }


  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:trip)).to be_valid
  end

  it 'will be an instance of Trip' do
    expect(subject).to be_an_instance_of Trip
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_trip = FactoryGirl.build(:trip)

    it 'will not have an id' do
      expect(the_trip.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_trip.paid_transport = nil
      expect(the_trip.valid?).to be_false
      expect(the_trip.persisted?).to_not be_true
    end
  end
end
