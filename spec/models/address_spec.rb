require 'spec_helper'

describe Address do
  subject { FactoryGirl.build_stubbed(:address) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:address)).to be_valid
  end

  it 'will be an instance of Address' do
    expect(subject).to be_an_instance_of Address
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_address = FactoryGirl.build(:address)

    it 'will not have an id' do
      expect(the_address.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_address.address1 = nil
      the_address.save
      expect(the_address.valid?).to be_false
      expect(the_address.persisted?).to_not be_true
    end
  end
end
