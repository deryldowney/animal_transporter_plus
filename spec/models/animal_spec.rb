require 'spec_helper'

describe Animal do
  subject { FactoryGirl.build_stubbed(:animal) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:animal)).to be_valid
  end

  it 'will be an instance of Animal' do
    expect(subject).to be_an_instance_of Animal
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_animal = FactoryGirl.build(:animal)

    it 'will not have an id' do
      expect(the_animal.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_animal.species = nil
      the_animal.save
      expect(the_animal.valid?).to be_false
      expect(the_animal.persisted?).to_not be_true
    end
  end

end
