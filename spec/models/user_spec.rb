require 'spec_helper'

describe User do

  subject { FactoryGirl.build_stubbed(:user) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:user)).to be_valid
  end

  it 'will be an instance of User' do
    expect(subject).to be_an_instance_of User
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do

    let(:subject) { FactoryGirl.build(:user, username: nil) }

    it 'will not have an id' do
      expect(subject.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      expect(subject.valid?).to be_false
      expect(subject.persisted?).to_not be_true
    end
  end

end
