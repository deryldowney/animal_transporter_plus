require 'spec_helper'

describe Transporter do

  subject { FactoryGirl.build_stubbed(:transporter) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:transporter)).to be_valid
  end

  it 'will be an instance of Transporter' do
    expect(subject).to be_an_instance_of Transporter
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_transporter = FactoryGirl.build(:transporter)

    it 'will not have an id' do
      expect(the_transporter.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_transporter.first_name = nil
      the_transporter.save
      expect(the_transporter.valid?).to be_false
      expect(the_transporter.persisted?).to_not be_true
    end
  end

end
