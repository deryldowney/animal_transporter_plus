require 'spec_helper'

describe Adopter do
  subject { FactoryGirl.build_stubbed(:adopter) }

  it 'will have a valid Factory' do
    expect(FactoryGirl.create(:adopter)).to be_valid
  end

  it 'will be an instance of Adopter' do
    expect(subject).to be_an_instance_of Adopter
  end

  context 'when persisted' do

    it 'will have an id assigned' do
      expect(subject.id).to_not be_blank
    end

    it 'will be valid' do
      expect(subject.valid?).to be_true
    end
  end

  context 'when instantiated' do
    the_adopter = FactoryGirl.build(:adopter)

    it 'will not have an id' do
      expect(the_adopter.id).to be_blank
    end

    it 'should fail validation if required info not present' do
      the_adopter.first_name = nil
      the_adopter.save
      expect(the_adopter.valid?).to be_false
      expect(the_adopter.persisted?).to_not be_true
    end
  end

end
