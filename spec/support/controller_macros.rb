module ControllerMacros
  def login_as_user
    @current_user = FactoryGirl.build_stubbed(:user)
    session[:user_id] = @current_user.id
    session[:expires_at] = 4.hours.from_now
    User.should_recieve(:find).once.with.(@current_user.id).and_return(@current_user)
  end
end
