# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip do
    start_address
    end_address
    estimated_mileage 1
    actual_mileage 1

    trait :with_legs do
      after :create do |trip|
        FactoryGirl.create_list(leg: 3, trip: trip)
      end
    end
  end
end
