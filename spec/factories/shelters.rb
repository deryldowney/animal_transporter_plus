# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :shelter do
    name "Shelter"
    phone_number "606-555-1212"
    contact_person "Shelter Contact"
    address
  end
end
