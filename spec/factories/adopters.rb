# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :adopter do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    phone_number { Faker::PhoneNumber.short_phone_number }
    vetted false
    banned false
    address
  end
end
