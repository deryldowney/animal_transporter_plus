# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :animal do
    species "Dog"
    age 2
    weight 120
    sex "female"
    breed "Rottweiler"
    altered false
    aggressive false
    health_certificate false
    diseases "None"
  end
end
