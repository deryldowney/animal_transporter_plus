# Read about factories at https://github.com/thoughtbot/factory_girl
require 'ffaker'

#Faker::Config.locale = 'en_US'

FactoryGirl.define do
  factory :transporter do
    first_name {Faker::Name.first_name}
    last_name {Faker::Name.last_name}
    email_address {Faker::Internet.email}
    primary_phone { Faker::PhoneNumber.short_phone_number }
    secondary_phone { Faker::PhoneNumber.short_phone_number }
    paid_transport false
    registered false
    address
  end
end
