# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :leg do
    transporter_id 1
    estimated_mileage 1
    actual_mileage 1
    paid false
    start_address
    end_address
    transporter
  end
end
