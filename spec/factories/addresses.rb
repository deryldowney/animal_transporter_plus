# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address, aliases: [:start_address, :end_address] do
    address1 { Faker::AddressUS.street_address }
    address2 nil
    address3 nil
    city { Faker::AddressUS.city }
    state { Faker::AddressUS.state }
    zip { Faker::AddressUS.zip_code }
  end
end
