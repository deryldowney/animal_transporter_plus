# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :puller do
    first_name "MyString"
    last_name "MyString"
    phone_number "MyString"
    email_address "MyString"
    address
    rescue_group
  end
end
