# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    full_name { Faker::Name.first_name + " " + Faker::Name.last_name }
    username { Faker::Internet.user_name }
    email_address { Faker::Internet.email }
    role "rescue_group"
    address
    phone_number { Faker::PhoneNumber.short_phone_number }
    password "password"
    password_confirmation "password"
  end

end
