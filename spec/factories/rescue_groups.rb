# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rescue_group do
    name "Rescue Group"
    phone_number { Faker::PhoneNumber.short_phone_number }
    contact { Faker::Name.name }
    emergency_contact { Faker::Name.name }
    email_address { Faker::Internet.email }
    address
 end
end
