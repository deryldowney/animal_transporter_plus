require "spec_helper"

feature "Rescue Group", js: true do
  before(:all) do
    @current_user = FactoryGirl.build_stubbed(:user)
  end

  scenario "User should able to register as a new Rescue Group",  js: true do
    visit "/register"
    fill_in "Full Name:", with: @current_user.full_name
    fill_in "Username:", with: "testuser"
    fill_in "Email Address:", with: @current_user.email_address
    fill_in "Phone Number:", with: @current_user.phone_number
    fill_in "Address1", with: @current_user.address.address1
    fill_in "City", with: @current_user.address.city
    fill_in "State", with: @current_user.address.state
    fill_in "Zip Code", with: @current_user.address.zip
    select("RescueGroup", from: "user_role")
    fill_in "Password:", with: "password"
    fill_in "Confirm Password:", with: "password"

    click_on "Create User"

    expect(@current_user.role).to_not be_blank
    expect(@current_user.role).to have_text(User::ROLES_FOR_DISPLAY[:rescue_group])
    expect(page).to have_content("successfully signed up")

    # Right here, we should be redirected by the application to new_user_rescue_group_path
    # We should be creating a new RescueGroup, contexted to @current_user, which should be
    # the new User we initially created. 'session[:user_id] has already been set to that.
    #
    # The logic is that if the user signed up with the role 'RescueGroup', immediately after
    # their User is created, the app redirs to creating their rescue_group, susequently scoping
    # and redirecting them, to their dashboard (to|as) that rescue_group once that is completed.
    # (NOTE: All _User_ data is under "Account" link. Separate entities.)
    #
    # This works fine in production, but not in development
    expect(page).to have_content("New Rescue Group")


    fill_in "Name of Rescue", with: "Rescue Group"
    fill_in "Phone Number", with: "555-444-3321"
    fill_in "Email Address", with: "rescue@rescue.com"
    fill_in "Primary Contact", with: "Joe Contact"
    fill_in "Emergency Contact", with: "Mary Emergency"
    fill_in "Address Line 1", with: @current_user.address.address1
    fill_in "Address Line 2", with: @current_user.address.address2
    fill_in "City", with: @current_user.address.city
    fill_in "State", with: @current_user.address.state
    fill_in "Zip Code", with: @current_user.address.zip
    click_on("Create Rescue group")

    expect(page).to have_content("Rescue Group was successfully created.")
  end

  scenario "User should see their Rescue Group Dashboard after login", js: true do
    visit "/login"
    fill_in "Username", with: "testuser"
    fill_in "Password", with: "password"

    click_on("Log In")

    expect(page.current_path).to have_content("/dashboard")
    expect(page).to have_content("Successfully logged in")
    expect(page).to have_content("Rescue Group's Dashboard")
  end

end

