require 'spec_helper'

feature "Manage Shelters" do
  scenario "can create a Shelter" do
    pending "REWRITE REQUIRED! Insufficiently scoped."

    visit "/shelters"
    click_link "Create a New Shelter"

    fill_in "Full Name", with: "Petey's Shelter"
    fill_in "Phone Number", with: "606-555-1001"
    fill_in "Contact Person", with: "Joe Shelter"
    fill_in "Address Line 1", with: "101 Some Street"
    fill_in "City", with: "Tollesboro"
    fill_in "State", with: "KY"
    fill_in "Zip", with: "41179"

    click_button "Create Shelter"

    expect(page).to have_content('Shelter was successfully created.')
  end

end
