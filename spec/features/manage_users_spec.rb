require "spec_helper"

feature "User Registration" do

  before(:all) do
    visit root_url

    click_on "Register"

    expect(page).to have_content("New Account Registration")

    fill_in "Full Name", with: "Joe Rescue"
    fill_in "Username", with: "joerescue"
    fill_in "Phone Number", with: "606-555-1212"
    fill_in "Email Address", with: "ppr@kyspca.org"
    fill_in "Address1", with: "123 Any Street"
    fill_in "City", with: "Tollesboro"
    fill_in "State", with: "KY"
    fill_in "Zip Code", with: "41189"
    select "Transporter", from: "user_role"
    fill_in "Password:", with: "password", exact: true
    fill_in "Confirm Password:", with: "password", exact: true

    click_on "Create User"
    click_on "Sign Out"
  end

  scenario "User can register" do
    visit root_url

    click_on "Register"

    expect(page).to have_content("New Account Registration")

    fill_in "Full Name", with: "Joe Puma's Rescue"
    fill_in "Username", with: "joepumarescue"
    fill_in "Phone Number", with: "606-515-1234"
    fill_in "Email Address", with: "joepuma@kyspca.org"
    fill_in "Address1", with: "123 Any Street"
    fill_in "City", with: "Tollesboro"
    fill_in "State", with: "KY"
    fill_in "Zip Code", with: "41189"
    select "Transporter", from: "user_role"
    fill_in "Password:", with: "password", exact: true
    fill_in "Confirm Password:", with: "password", exact: true

    click_on "Create User"

    expect(page).to have_content("Dashboard")
  end

  scenario "User can access their personal Account page" do
    visit root_url
    click_on "Sign In"

    fill_in "Username", with: "joerescue"
    fill_in "Password", with: "password", exact: true
    click_on "Log In"
    click_on "Account"

    expect(page).to have_content("User Account")
    expect(page).to have_content("Edit User Information")
  end
  scenario "Users can Sign In" do
    visit "/"

    click_link "Sign In"

    fill_in 'Username', with: "joerescue"
    fill_in 'Password', with: "password", exact: true
    click_button 'Log In'

    expect(page).to have_content("Successfully logged in as")

  end

  scenario "Users can Sign Out" do
    visit "/"

    click_link "Sign In"

    fill_in 'Username', with: "joerescue"
    fill_in 'Password', with: "password", exact: true
    click_button 'Log In'

    click_link "Sign Out"

    expect(page).to have_content("Logged Out")
  end
end
