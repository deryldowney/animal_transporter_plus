module ApplicationHelper
  def title(*parts)
    unless parts.empty?
      content_for(:title) do
        (parts << "Transporters Plus").join(" - ")
      end
    end
  end
end
