class RescueGroup < ActiveRecord::Base

  validates :name,
            :email_address,
            :contact,
            :emergency_contact, presence: true

  validates :phone_number, presence: true, format: { with: /\A\d{3}\-(\d{3})-(\d{4})\Z/, message: "must be in the format of 555-555-1212" }

  has_one :address, dependent: :destroy
  accepts_nested_attributes_for :address

  belongs_to :user
  has_many :shelters     # Each rescue_group usually deals with multiple shelters

  has_many :animals      # RescueGroups obviously deal with many animals. Each animal
                         # needs to have which rescue_group it came from originally,
                         # and, if needed, how many others were involved.

  has_many :pullers      # RescueGroups have people designated as (a) Puller(s) that go to the Shelter(s)
                         # to physically take control of the Animal(s).

  has_many :adopters, through: :animals # RescueGroups track Adopter(s) for Animal(s) through the Animal(s)

  has_many :trips # RescueGroups need to track Trip(s) in order to see where (a) Animal(s) went.

  has_and_belongs_to_many :transporters # A RescueGroup usually has many transporters associated with it
                                        # Note that (a) Transporter(s) can work with many RescueGroups
                                        # at the same time, showing on multiple RescueGroup rolls.

end
