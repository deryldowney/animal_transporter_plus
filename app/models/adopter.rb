class Adopter < ActiveRecord::Base

  validates :first_name,
            :last_name,
            :phone_number, presence: true

  validates_inclusion_of :vetted, :banned, in: [true, false]

  has_one :address, dependent: :destroy
  accepts_nested_attributes_for :address

  has_one :animal, through: :rescue_group, dependent: :destroy  # Usually, but not always, a single animal goes to a single adopter
  accepts_nested_attributes_for :animal                         # Not sure if has_one is appropriate due to edge case.

  belongs_to :rescue_group

end
