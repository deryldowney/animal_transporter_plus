class Address < ActiveRecord::Base

  validates :address1,
           :city,
           :state,
           :zip, presence: true

  belongs_to :shelter
  belongs_to :rescue_group
  belongs_to :transporter
  belongs_to :puller
  belongs_to :adopter
  belongs_to :user

end
