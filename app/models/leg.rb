class Leg < ActiveRecord::Base

  has_one :start_address, foreign_key: "trip_id", class_name: "Address"
  has_one :end_address, foreign_key: "trip_id", class_name: "Address"

  validates :estimated_mileage, :actual_mileage, presence: true
  validates_inclusion_of :paid, in: [true, false]

  belongs_to :trip
  belongs_to :transporter

end
