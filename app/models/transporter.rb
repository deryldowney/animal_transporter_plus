class Transporter < ActiveRecord::Base

  validates :first_name,
            :last_name,
            :email_address,
            :primary_phone, presence: true

  validates_inclusion_of :paid_transport, :registered, in: [true, false]

  has_one :address
  accepts_nested_attributes_for :address

  has_and_belongs_to_many :rescue_groups  # more than one rescue_group they can transport for
                     # more than one rescue_group as well.
  has_many :trips    # handles many trips for different animals
  has_many :legs     # handles many legs for different trips

end
