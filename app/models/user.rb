class User < ActiveRecord::Base
  has_secure_password

  ROLES_FOR_DISPLAY = { "RescueGroup" => "rescue_group",
                        "Transporter" => "transporter" }
  ROLES = ROLES_FOR_DISPLAY.values

  validates :role, inclusion: { in: ROLES, allow_blank: false }

  validates :username, presence: true, uniqueness: true

  validates :email_address, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ }

  validates :phone_number, presence: true, format: { with: /\A\d{3}\-(\d{3})-(\d{4})\Z/, message: "must be in the format of 555-555-1212" }

  validates :password, length: { minimum: 8 }

  has_many :rescue_groups, dependent: :destroy
  has_many :shelters, through: :rescue_groups, dependent: :destroy

  has_one :address, class_name: "Address", dependent: :destroy
  accepts_nested_attributes_for :address, allow_destroy: true

  scope :rescue_groups, -> { where(RescueGroup.where(user_id: self).load) }

end
