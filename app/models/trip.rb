class Trip < ActiveRecord::Base


  has_one :start_address, foreign_key: "trip_id", class_name: "Address"
  has_one :end_address, foreign_key: "trip_id", class_name: "Address"

  validates_inclusion_of :paid_transport, in: [true, false]

  has_many :legs            # Usually a trip is broken down into legs, using multiple transporters
  has_many :transporters    # Usually, but not always, multiple transporters involved
  has_many :animals         # Usually, but not always, A animal sent, but can be more
  has_many :rescue_groups   # Usually A rescue_group involved, but can be more (cross-export)
  has_many :legs            # A trip can have 1 or many legs (depends how many drivers want to make the total trip)
end
