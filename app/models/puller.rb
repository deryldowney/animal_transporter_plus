class Puller < ActiveRecord::Base

  validates :first_name,
            :last_name,
            :phone_number, presence: true

  belongs_to :rescue_group

  has_one :address, dependent: :destroy
  accepts_nested_attributes_for :address

end
