class Shelter < ActiveRecord::Base

  validates :name,
            :phone_number,
            :contact_person, presence: true

  has_many :animals, dependent: :destroy  # Has many animals obviously. Also deals with multiple rescue_groups
                                          # but RARELY deals with transporters. Thats the rescue_group's job.
                                          # Also, the shelter doesn't list the rescue_groups. its just that many
                                          # rescue_groups happen to deal with the same shelter.
  accepts_nested_attributes_for :animals, reject_if: :all_blank, allow_destroy: true

  has_one :address, dependent: :destroy
  accepts_nested_attributes_for :address

  belongs_to :rescue_group
end
