class Animal < ActiveRecord::Base

  validates :species,
            :age,
            :sex,
            :breed,
            :diseases, presence: true


  validates_inclusion_of :altered, :aggressive, :health_certificate, in: [true, false]

  belongs_to :adopter        # Only one adopter is listed even if several will share the
                          # animal. That detail is for the adopters to figure out.

  belongs_to :rescue_group   # One rescue_group got the animal initially. Its only with the trip
                          # generally, that multiple rescue_groups get involved.
  belongs_to :shelter        # Each animal is rescued from a single shelter, obviously.

end
