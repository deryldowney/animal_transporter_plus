class LegsController < ApplicationController
  def index
    @legs = Leg.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @legs }
    end
  end

  def show
    @leg = Leg.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @leg }
    end
  end

  def new
    @leg = Leg.new
    @leg.build_start_address
    @leg.build_end_address

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @leg }
    end
  end

  def create
    @leg = Leg.new(params[:leg])

    respond_to do |format|
      if @leg.save
        flash[:notice] = 'Leg was successfully created.'
        format.html { redirect_to(@leg) }
        format.xml  { render :xml => @leg, :status => :created, :location => @leg }
      else
        flash[:error] = 'OOPS! Leg failed to create.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @leg.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @leg = Leg.find(params[:id])

    respond_to do |format|
      if @leg.update_attributes(params[:leg])
        flash[:notice] = 'Successfully updated Leg!'
        format.html { redirect_to(@leg) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! Leg could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @leg.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @leg = Leg.find(params[:id])
  end

  def destroy
    @leg = Leg.find(params[:id])
    @leg.destroy

    respond_to do |format|
      flash[:notice] = 'Leg successfully deleted!'
      format.html { redirect_to(legs_url) }
      format.xml  { head :ok }
    end
  end

  private

    def leg_params
      params.require(:leg).permit(:transporter_id,:estimated_mileage, :actual_mileage, :paid, :trip_id, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip])
    end

end
