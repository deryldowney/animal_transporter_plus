class TripsController < ApplicationController
  def index
    @trips = Trip.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @trips }
    end
  end

  def show
    @trip = Trip.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @trip }
    end
  end

  def new
    @trip = Trip.new
    @trip.build_start_address
    @trip.build_end_address

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @trip }
    end
  end

  def create
    @trip = Trip.new(params[:trip])

    respond_to do |format|
      if @trip.save
        flash[:notice] = 'Trip was successfully created.'
        format.html { redirect_to(@trip) }
        format.xml  { render :xml => @trip, :status => :created, :location => @trip }
      else
        flash[:error] = 'OOPS! Trip failed to create.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @trip.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @trip = Trip.find(params[:id])

    respond_to do |format|
      if @trip.update_attributes(params[:trip])
        flash[:notice] = 'Successfully updated Trip!'
        format.html { redirect_to(@trip) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! Trip could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @trip.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @trip = Trip.find(params[:id])
  end

  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy

    respond_to do |format|
      flash[:notice] = 'Trip successfully deleted!'
      format.html { redirect_to(trips_url) }
      format.xml  { head :ok }
    end
  end

  private

    def trip_params
      params.require(:trip).permit(:estimated_mileage, :actual_mileage, :paid_transport, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip])
    end

end
