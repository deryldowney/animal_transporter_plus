class PagesController < ApplicationController
  def index
  end

  def about
  end

  def services
    # All stuff for services like vans, planes, etc
    # Also for stuff like vet insurance for animal, and vet clinics
  end

  def equipment
    # All stuff for travel equipment goes here.
    # Stuff like cages, leashes, bowls, puppy pads, and bleach
  end

  def contact_us
  end

  private

  def register
    # Code will go here for users to register for an account
    #  /register will now go to users#new for Sign up
  end

  def account
    #current_user.address = Address.where(user_id: current_user.id).first
  end
end
