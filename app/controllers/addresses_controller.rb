class AddressesController < ApplicationController
  def new
  end

  def create
  end

  def index
  end

  def show
  end

  def edit
    @address = Address.where(user_id: params[:user_id]).first
  end

  def update
    if current_user.update_attributes(params[:address])
      redirect_to account_path
    end
  end

  def delete
  end
end
