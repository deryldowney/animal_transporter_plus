class AnimalsController < ApplicationController
  def new
    @animal = Animal.new
  end

  def create
    @animal = Animal.new(params[:animal])

    if @animal.save!
      flash[:notice] = "Animal has been created."
      redirect_to @animal
    else
      flash[:error] = "ERROR! Animal was not created!"
      render action: "new"
    end
  end

  def index
    @animals = Animal.all
  end

  def show
    @animal = Animal.find(params[:id])
  end

  def edit
    @animal = Animal.find(params[:id])
  end

  def update
    @animal = Animal.find(params[:id])
    if @animal.update_attributes(params[:animal])
      flash[:notice] = "Successfully updated Animal!"
      redirect_to(@animal)
    else
      render action: "edit"
    end
  end

  def destroy
    Animal.find(params[:id]).destroy
    flash[:notice] = "Animal successfully deleted!"
    redirect_to animals_path
  end

  private

    def animal_params
      params.require(:animal).permit(:species, :age, :weight, :sex, :breed, :altered, :aggressive, :health_certificate, :diseases, :adopter, :shelter)
    end
end
