class RescueGroupsController < ApplicationController
  def index
    @rescue_groups = RescueGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @rescue_groups }
    end
  end

  def show
    @rescue_group = RescueGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @rescue_group }
    end
  end

  def new
    @rescue_group = current_user.rescue_groups.new
    @rescue_group.build_address

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @rescue_group }
    end
  end

  def create
    @rescue_group = current_user.rescue_groups.create!(params[:rescue_group])

    respond_to do |format|
      if @rescue_group.save
        flash[:notice] = 'Rescue Group was successfully created.'
        format.html { redirect_to(@rescue_group) }
        format.xml  { render :xml => @rescue_group, :status => :created, :location => @rescue_group }
      else
        flash[:error] = 'OOPS! Rescue Group failed to create.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @rescue_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @rescue_group = RescueGroup.find(params[:id])

    respond_to do |format|
      if @rescue_group.update_attributes(params[:rescue_group])
        flash[:notice] = 'Successfully updated Rescue Group!'
        format.html { redirect_to(@rescue_group) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! Rescue Group could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @rescue_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @rescue_group = RescueGroup.find(params[:id])
  end

  def destroy
    @rescue_group = RescueGroup.find(params[:id])
    @rescue_group.destroy

    respond_to do |format|
      flash[:notice] = 'Rescue Group successfully deleted!'
      format.html { redirect_to(rescue_groups_url) }
      format.xml  { head :ok }
    end
  end

  private

  def rescue_group_params
      params.require(:rescue_group).permit(:name, :phone_number, :email_address,:contact, :emergency_contact, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip, :user_id])
    end

end
