class SheltersController < ApplicationController
  def index
    @rescue_group = RescueGroup.find(params[:rescue_group_id])

    @shelters = @rescue_group.shelters.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @shelters }
    end
  end

  def show
    @shelter = Shelter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @shelter }
    end
  end

  def new
    # We only pass in rescue_group_id. So have to get the right rescue first
    @rescue_group = RescueGroup.find(params[:rescue_group_id])

    # Now this should all work
    @shelter = @rescue_group.shelters.new
    @shelter.build_address
    @shelter.animals.build

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @shelter }
    end
  end

  def create
    @rescue_group = RescueGroup.find(params[:rescue_group_id])

    @shelter = @rescue_group.shelters.create(params[:shelter])

    if @shelter.valid? == true  && @shelter.save!
      respond_to do |format|
        format.html { redirect_to rescue_group_shelters_path(@rescue_group), notice: "Shelter #{@shelter.name} was successfully created." }
        format.xml  { render :xml => @shelter, :status => :created, :location => @shelter }
      end
    else
      flash[:error] = 'OOPS! Shelter failed to create.'
      render action: 'new'
    end
  end

  def update
    @shelter = Shelter.find(params[:id])

    respond_to do |format|
      if @shelter.valid? == true && @shelter.update_attributes(params[:shelter])
        flash[:notice] = 'Successfully updated Shelter!'
        format.html { redirect_to(@shelter) }
        format.xml  { head :ok }
      else
        flash.now[:error] = "OOPS! Shelter could not be updated."
        flash[:error] = "OOPS! Shelter could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shelter.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @shelter = Shelter.find(params[:id])
  end

  def destroy
    @shelter = Shelter.find(params[:id])
    @shelter.destroy

    respond_to do |format|
      flash[:notice] = 'Shelter successfully deleted!'
      format.html { redirect_to(rescue_group_shelters_path(@shelter.rescue_group_id)) }
      format.xml  { head :ok }
    end
  end

  private

  def shelter_params
    params.require(:shelter).permit(:name, :phone_number, :contact_person, :rescue_group_id, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip], :animals_attributes[:id, :species, :age, :weight, :sex, :breed, :altered, :aggressive, :health_certificate, :diseases, :shelter_id, :rescue_group_id, :adopter_id])
  end
end

