class ApplicationController < ActionController::Base
  include SignedForm::ActionController::PermitSignedParams

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    def current_user_rescue_group
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
        # Change current_user to be the rescue_group the User created.
        # NOTE: this is done so we can keep User available for User account management
        # but for daily operations they need to be working as the Rescue they created.
      if @current_user.role == "rescue_group" && @current_user.rescue_groups.any?
        @current_user = RescueGroup.find_by(user_id: session[:user_id]) if session[:user_id]
      else
        @current_user
      end
    end

    helper_method :current_user, :current_user_rescue_group

end
