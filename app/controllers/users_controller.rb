class UsersController < ApplicationController

  def new
    @user = User.new
    @user.build_address
  end

  def edit
    @user = User.find(params[:id])
  end

  def index

  end

  def show
  end

  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = 'Successfully updated User!'
        format.html { redirect_to account_path(@user) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! User could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      session[:user_id] = @user.id

      if @user.role == "rescue_group"
        flash[:notice] = "You have successfully signed up!"
        redirect_to new_user_rescue_group_path(@user)
      else
        redirect_to dashboard_path, notice: "You have successfully signed up!"
      end
    else
      render 'new'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      flash[:notice] = 'User successfully deleted!'
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end

  private

    def user_params
      params(:user).permit( :full_name, :username, :email_address, :role, :phone_number, :password, :password_confirmation, :type, :address_attributes [ :id, :address1, :address2, :address3, :city, :state, :zip ] )
    end
end
