class TransportersController < ApplicationController
  def index
    @transporters = Transporter.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @transporters }
    end
  end

  def show
    @transporter = Transporter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @transporter }
    end
  end

  def new
    @transporter = Transporter.new
    @transporter.build_address

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @transporter }
    end
  end

  def create
    @transporter = Transporter.new(params[:transporter])

    respond_to do |format|
      if @transporter.save
        flash[:notice] = 'Transporter was successfully created.'
        format.html { redirect_to(@transporter) }
        format.xml  { render :xml => @transporter, :status => :created, :location => @transporter }
      else
        flash[:error] = 'OOPS! Transporter failed to create.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @transporter.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @transporter = Transporter.find(params[:id])

    respond_to do |format|
      if @transporter.update_attributes(params[:transporter])
        flash[:notice] = 'Successfully updated Transporter!'
        format.html { redirect_to(@transporter) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! Transporter could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @transporter.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @transporter = Transporter.find(params[:id])
  end

  def destroy
    @transporter = Transporter.find(params[:id])
    @transporter.destroy

    respond_to do |format|
      flash[:notice] = 'Transporter successfully deleted!'
      format.html { redirect_to(transporters_url) }
      format.xml  { head :ok }
    end
  end

  private

    def transporter_params
      params.require(:transporter).permit(:first_name, :last_name, :email_address, :primary_phone, :secondard_phone, :paid_transport, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip])
    end

end
