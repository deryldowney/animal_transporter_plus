class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(username: params[:username])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to dashboard_path, notice: "Successfully logged in as #{current_user.username}!"
    else
      flash[:error] = "Username or password is invalid!"
      render "new"
    end
  end

# def delete
#     #session = Session.find(params[:session])
#     session[:user_id] = nil
#     redirect_to dashboard_path, notice: "Logged Out!"
# end

  def destroy
    #session = Session.find(params[:session])
    session[:user_id] = nil
    redirect_to dashboard_path, notice: "Logged Out!"
  end
end
