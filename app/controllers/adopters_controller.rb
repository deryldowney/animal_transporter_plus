class AdoptersController < ApplicationController
  def index
    @adopters = Adopter.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @adopters }
    end
  end

  def show
    @adopter = Adopter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @adopter }
    end
  end

  def new
    @adopter = current_user.adopters.new
    @adopter.build_address

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @adopter }
    end
  end

  def create
    @adopter = current_user.adopters.new(params[:adopter])

    respond_to do |format|
      if @adopter.save
        flash[:notice] = 'Adopter was successfully created.'
        format.html { redirect_to(@adopter) }
        format.xml  { render :xml => @adopter, :status => :created, :location => @adopter }
      else
        flash[:error] = 'OOPS! Adopter failed to create.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @adopter.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @adopter = Adopter.find(params[:id])

    respond_to do |format|
      if @adopter.update_attributes(params[:adopter])
        flash[:notice] = 'Successfully updated Adopter!'
        format.html { redirect_to(@adopter) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! Adopter could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @adopter.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @adopter = Adopter.find(params[:id])
  end

  def destroy
    @adopter = Adopter.find(params[:id])
    @adopter.destroy

    respond_to do |format|
      flash[:notice] = 'Adopter successfully deleted!'
      format.html { redirect_to(adopters_url) }
      format.xml  { head :ok }
    end
  end

  private

    def adopter_params
      params.require(:adopter).permit(:first_name, :last_name, :phone_number, :vetted, :banned, :rescue_group_id, :trip_id, :animal_id, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip])
    end

end
