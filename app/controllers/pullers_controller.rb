class PullersController < ApplicationController
  def index
    @pullers = Puller.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pullers }
    end
  end

  def show
    @puller = Puller.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @puller }
    end
  end

  def new
    @puller = Puller.new
    @puller.build_address

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @puller }
    end
  end

  def create
    @puller = Puller.new(params[:puller])

    respond_to do |format|
      if @puller.save
        flash[:notice] = 'Puller was successfully created.'
        format.html { redirect_to(@puller) }
        format.xml  { render :xml => @puller, :status => :created, :location => @puller }
      else
        flash[:error] = 'OOPS! Puller failed to create.'
        format.html { render :action => "new" }
        format.xml  { render :xml => @puller.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @puller = Puller.find(params[:id])

    respond_to do |format|
      if @puller.update_attributes(params[:puller])
        flash[:notice] = 'Successfully updated Puller!'
        format.html { redirect_to(@puller) }
        format.xml  { head :ok }
      else
        flash[:error] = "OOPS! Puller could not be updated."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @puller.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @puller = Puller.find(params[:id])
  end

  def destroy
    @puller = Puller.find(params[:id])
    @puller.destroy

    respond_to do |format|
      flash[:notice] = 'Puller successfully deleted!'
      format.html { redirect_to(pullers_url) }
      format.xml  { head :ok }
    end
  end

  private

    def puller_params
      params.require(:puller).permit(:name, :phone_number, :contact_person, :address_attributes[:id, :address1, :address2, :address3, :city, :state, :zip])
    end

end
