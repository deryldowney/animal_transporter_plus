AnimalTransporterPlus::Application.routes.draw do

  # For authentication system
  resources :users do
    resource :address
    resources :rescue_groups
    resources :shelters
    resources :transporters
  end
  resources :sessions

  # Listed in downward spiral order of operations. Ex: Shelter(s) have Animal(s) the Puller(s) get
  # and give to the RescueGroup(s) who create Trip(s), made of Leg(s) and Transporter(s), in order
  # to get the Animal(s) to Adopter(s).

  resources :rescue_groups do
    resources :adopters
    resources :shelters
    resources :animals do
      resources :shelters
      resource  :adopter
    end
    resources :pullers
    resources :transporters
    resources :trips do
      resources :legs
    end
  end

  # Dashboard for each user. This will be scoped to A) the particular user, and B) their role
  resource :dashboard

  # Application root
  root 'pages#index', only: :get

  get "/about" => "pages#about"
  get "/services" => "pages#services"
  get "/equipment" => "pages#equipment"
  get "/register" => "users#new"
  get "/contact_us" => "pages#contact_us"
  get "/dashboard" => "dashboard#show"
  get '/account' => "pages#account"
  get '/login', to: 'sessions#new', as: 'login'
  get '/logout', to: 'sessions#destroy', as: 'logout'
end
