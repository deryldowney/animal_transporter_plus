class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :username
      t.string :email_address
      t.string :phone_number
      t.string :role
      t.string :password_digest

      t.timestamps

    end

    add_index(:users, :id, unique: true)
    add_index(:users, :role)
  end
end
