class CreateRescueGroups < ActiveRecord::Migration
  def change
    create_table :rescue_groups do |t|
      t.string :name
      t.string :phone_number
      t.string :email_address
      t.string :contact
      t.string :emergency_contact
      t.references :user
      t.references :transporter

      t.timestamps
    end

    add_index(:rescue_groups, :id, unique: true)
    add_index(:rescue_groups, :transporter_id)
    add_index(:rescue_groups, :user_id)
  end
end
