class CreateAdopters < ActiveRecord::Migration
  def change
    create_table :adopters do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.references :address
      t.boolean :vetted, default: false
      t.boolean :banned, default: false
      t.references :rescue_group
      t.references :trip

      t.timestamps
    end
  end
end
