class CreateTransporters < ActiveRecord::Migration
  def change
    create_table :transporters do |t|
      t.string :first_name
      t.string :last_name
      t.string :email_address
      t.string :primary_phone
      t.string :secondary_phone
      t.boolean :paid_transport, default: false
      t.references :trip
      t.references :rescue_group
      t.boolean :registered, default: false

      t.timestamps
    end

    add_index(:transporters, :id, unique: true)
    add_index(:transporters, :trip_id)
    add_index(:transporters, :rescue_group_id)
  end
end
