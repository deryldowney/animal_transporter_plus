class CreateShelters < ActiveRecord::Migration
  def change
    create_table :shelters do |t|
      t.string :name
      t.string :phone_number
      t.string :contact_person
      t.references :rescue_group
      t.references :user

      t.timestamps
    end
  end
end
