class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :address1
      t.string :address2
      t.string :address3
      t.string :city
      t.string :state
      t.string :zip

      t.integer :shelter_id
      t.integer :rescue_group_id
      t.integer :transporter_id
      t.integer :adopter_id
      t.integer :puller_id
      t.integer :leg_id
      t.integer :trip_id
      t.integer :user_id
      t.timestamps
    end
  end
end
