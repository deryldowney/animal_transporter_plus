class CreateAnimals < ActiveRecord::Migration
  def change
    create_table :animals do |t|
      t.string :species
      t.integer :age
      t.integer :weight
      t.string :sex
      t.string :breed
      t.boolean :altered
      t.boolean :aggressive
      t.boolean :health_certificate
      t.text :diseases
      t.references :shelter
      t.references :rescue_group
      t.references :adopter

      t.timestamps
    end
  end
end
