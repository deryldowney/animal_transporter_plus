class CreateRescueGroupsTransportersJoinTable < ActiveRecord::Migration
  def change
    create_join_table :rescue_groups, :transporters, :id => false do |t|
      t.index :rescue_group_id
      t.index :transporter_id
    end
  end
end
