class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.integer :estimated_mileage
      t.integer :actual_mileage
      t.boolean :paid_transport, default: false
      t.references :rescue_group

      t.timestamps
    end
  end
end
