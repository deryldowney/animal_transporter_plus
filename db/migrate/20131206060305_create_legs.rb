class CreateLegs < ActiveRecord::Migration
  def change
    create_table :legs do |t|
      t.integer :estimated_mileage
      t.integer :actual_mileage
      t.boolean :paid
      t.references :trip
      t.references :transporter

      t.timestamps
    end
  end
end
