class CreatePullers < ActiveRecord::Migration
  def change
    create_table :pullers do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.string :email_address
      t.references :rescue_group

      t.timestamps
    end
  end
end
