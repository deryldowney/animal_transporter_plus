# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140104225512) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: true do |t|
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.integer  "shelter_id"
    t.integer  "rescue_group_id"
    t.integer  "transporter_id"
    t.integer  "adopter_id"
    t.integer  "puller_id"
    t.integer  "leg_id"
    t.integer  "trip_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "adopters", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.integer  "address_id"
    t.boolean  "vetted",          default: false
    t.boolean  "banned",          default: false
    t.integer  "rescue_group_id"
    t.integer  "trip_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "animals", force: true do |t|
    t.string   "species"
    t.integer  "age"
    t.integer  "weight"
    t.string   "sex"
    t.string   "breed"
    t.boolean  "altered"
    t.boolean  "aggressive"
    t.boolean  "health_certificate"
    t.text     "diseases"
    t.integer  "shelter_id"
    t.integer  "rescue_group_id"
    t.integer  "adopter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "legs", force: true do |t|
    t.integer  "estimated_mileage"
    t.integer  "actual_mileage"
    t.boolean  "paid"
    t.integer  "trip_id"
    t.integer  "transporter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pullers", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "email_address"
    t.integer  "rescue_group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rescue_groups", force: true do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "email_address"
    t.string   "contact"
    t.string   "emergency_contact"
    t.integer  "user_id"
    t.integer  "transporter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rescue_groups", ["id"], name: "index_rescue_groups_on_id", unique: true, using: :btree
  add_index "rescue_groups", ["transporter_id"], name: "index_rescue_groups_on_transporter_id", using: :btree
  add_index "rescue_groups", ["user_id"], name: "index_rescue_groups_on_user_id", using: :btree

  create_table "rescue_groups_transporters", id: false, force: true do |t|
    t.integer "rescue_group_id", null: false
    t.integer "transporter_id",  null: false
  end

  add_index "rescue_groups_transporters", ["rescue_group_id"], name: "index_rescue_groups_transporters_on_rescue_group_id", using: :btree
  add_index "rescue_groups_transporters", ["transporter_id"], name: "index_rescue_groups_transporters_on_transporter_id", using: :btree

  create_table "shelters", force: true do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "contact_person"
    t.integer  "rescue_group_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transporters", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email_address"
    t.string   "primary_phone"
    t.string   "secondary_phone"
    t.boolean  "paid_transport",  default: false
    t.integer  "trip_id"
    t.integer  "rescue_group_id"
    t.boolean  "registered",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transporters", ["id"], name: "index_transporters_on_id", unique: true, using: :btree
  add_index "transporters", ["rescue_group_id"], name: "index_transporters_on_rescue_group_id", using: :btree
  add_index "transporters", ["trip_id"], name: "index_transporters_on_trip_id", using: :btree

  create_table "trips", force: true do |t|
    t.integer  "estimated_mileage"
    t.integer  "actual_mileage"
    t.boolean  "paid_transport",    default: false
    t.integer  "rescue_group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "full_name"
    t.string   "username"
    t.string   "email_address"
    t.string   "phone_number"
    t.string   "role"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["id"], name: "index_users_on_id", unique: true, using: :btree
  add_index "users", ["role"], name: "index_users_on_role", using: :btree

end
