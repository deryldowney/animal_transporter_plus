MODEL DEFINITIONS FILE
======================


DEVELOPER NOTE
--------------
This file contains the Model definitions. This file is simply
a convenience file so folks don't have to jump through different
files trying to get a bird's eye view of all the models.

This will become the defacto place to look, in the end.

MODEL SPECS
-----------

    Address
      id: integer
      address1: string
      address2: string
      address3: string
      city: string
      state: string
      zip: string
      shelter_id: integer
      rescue_group_id: integer
      transporter_id: integer
      adopter_id: integer
      puller_id: integer
      leg_id: integer
      trip_id: integer
      created_at: datetime
      updated_at: datetime
      belongs_to :adopter
      belongs_to :puller
      belongs_to :rescue_group
      belongs_to :shelter
      belongs_to :transporter

    Adopter
      id: integer
      first_name: string
      last_name: string
      phone_number: string
      address_id: integer
      vetted: boolean
      banned: boolean
      animal_id: integer
      rescue_group_id: integer
      trip_id: integer
      created_at: datetime
      updated_at: datetime
      has_one :address
      has_one :animal

    Animal
      id: integer
      species: string
      age: integer
      weight: integer
      sex: string
      breed: string
      altered: boolean
      aggressive: boolean
      health_certificate: boolean
      diseases: text
      created_at: datetime
      updated_at: datetime
      has_one :adopter
      has_one :rescue_group
      has_one :shelter

    Leg
      id: integer
      trip_id: integer
      transporter_id: integer
      estimated_mileage: integer
      actual_mileage: integer
      paid: boolean
      created_at: datetime
      updated_at: datetime
      belongs_to :transporter
      belongs_to :trip
      has_one :end_address
      has_one :start_address

    Puller
      id: integer
      first_name: string
      last_name: string
      phone_number: string
      email_address: string
      rescue_group_id: integer
      created_at: datetime
      updated_at: datetime
      belongs_to :rescue_group
      has_one :address

    RescueGroup
      id: integer
      name: string
      phone_number: string
      email_address: string
      contact: string
      emergency_contact: string
      created_at: datetime
      updated_at: datetime
      has_many :animals
      has_many :shelters
      has_many :transporters
      has_one :address

    Shelter
      id: integer
      name: string
      phone_number: string
      contact_person: string
      created_at: datetime
      updated_at: datetime
      has_many :animals
      has_one :address

    Transporter
      id: integer
      first_name: string
      last_name: string
      email_address: string
      primary_phone: string
      secondary_phone: string
      paid_transport: boolean
      address_id: integer
      registered: boolean
      created_at: datetime
      updated_at: datetime
      has_many :animals
      has_many :legs
      has_many :rescue_groups
      has_many :trips
      has_one :address

    Trip
      id: integer
      estimated_mileage: integer
      actual_mileage: integer
      paid_transport: boolean
      created_at: datetime
      updated_at: datetime
      has_many :animals
      has_many :legs
      has_many :rescue_groups
      has_many :transporters
      has_one :end_address
      has_one :start_address
