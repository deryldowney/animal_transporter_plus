Transporter Plus [![Build Status](https://travis-ci.org/ddd/animal_transporter_plus.png?branch=master)](https://travis-ci.org/ddd/animal_transporter_plus)
================

Transporter Plus is an animal rescue web application for linking Rescues and animals with transport drivers.

Additional Install Notes
========================
Don't forget to run:

  echo "SignedForm.secret_key = '$(rake secret)'" > config/initializers/signed_form.rb

after bundling, and setting up the databases. If you forget, it will not run correctly.
This is for the 'signed_form' gem.

